package fr.o80.benchybencho;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mProgressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
        mProgressBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 24));
        mProgressBar.setIndeterminate(true);
        mProgressBar.setVisibility(View.GONE);

        ((ViewGroup)findViewById(android.R.id.content)).addView(mProgressBar);
    }

    @OnClick(R.id.button256Ko)
    void on256KoClicked() {
        download(256 * 1024);
    }

    @OnClick(R.id.button1Mo)
    void on1MoClicked() {
        download(1024 * 1024);
    }

    @OnClick(R.id.button10Mo)
    void on10MoClicked() {
        download(10 * 1024 * 1024);
    }

    void download(long size) {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url("http://android.olivierperez.fr/file.php?size=" + size)
                .build();

        mProgressBar.setVisibility(View.VISIBLE);
        Observable
                .defer(new Func0<Observable<Long>>() {
                    @Override
                    public Observable<Long> call() {
                        // TODO Tester si INTERNET est ouvert
                        try {
                            long start = System.currentTimeMillis();
                            Response response = client.newCall(request).execute();
                            response.body().string();
                            long end = System.currentTimeMillis();
                            return Observable.just(end - start);
                        } catch (IOException e) {
                            Log.e("TAG", e.getMessage());
                            throw new RuntimeException(e);
                        }
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnNext(new Action1<Long>() {
                    @Override
                    public void call(Long millis) {
                        Snackbar
                                .make(findViewById(android.R.id.content), String.format("Response in %dms", millis), Snackbar.LENGTH_LONG)
                                .show();
                    }
                })
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Snackbar
                                .make(findViewById(android.R.id.content), String.format("Error %s", throwable.getClass().getSimpleName()), Snackbar.LENGTH_LONG)
                                .show();
                    }
                })
                .doOnCompleted(new Action0() {
                    @Override
                    public void call() {
                        mProgressBar.setVisibility(View.GONE);
                    }
                })
                .subscribe();
    }
}
